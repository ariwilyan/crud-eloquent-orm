@extends('adminlte.master')

@section('title')
    Home Pertanyaan
@endsection

@section('content')
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Lists Pertanyaan</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            @if (session('success'))
                {{-- <div class="alert alert-success alert-dismissible fade show" role="alert">
                    {{ session('success') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div> --}}
                @push('scripts')
                    <script>
                        Swal.fire(
                            'Success!',
                            'Pertanyaan baru berhasil ditambahkan!',
                            'success'
                        )
                    </script>
                @endpush
            @endif
            @if (session('update'))
                @push('scripts')
                    {{-- <script>
                        const swalWithBootstrapButtons = Swal.mixin({
                            customClass: {
                                confirmButton: 'btn btn-success',
                                cancelButton: 'btn btn-danger'
                            },
                            buttonsStyling: false
                        })

                        swalWithBootstrapButtons.fire({
                            title: 'Apakah kamu yakin?',
                            text: "Data pertanyaan yang sudah dihapus tidak bisa dikembalikan lagi!",
                            icon: 'warning',
                            showCancelButton: true,
                            confirmButtonText: 'Ya, hapus!',
                            cancelButtonText: 'Batal',
                            reverseButtons: true
                        }).then((result) => {
                            if (result.isConfirmed) {
                                swalWithBootstrapButtons.fire(
                                    'Deleted!',
                                    'Data pertanyaan sudah terhapus.',
                                    'success'
                                )
                            } else if (
                                /* Read more about handling dismissals below */
                                result.dismiss === Swal.DismissReason.cancel
                            ) {
                                swalWithBootstrapButtons.fire(
                                    'Cancelled',
                                    'Data pertanyaan masih aman :)',
                                    'error'
                                )
                            }
                        })
                    </script> --}}
                    <script>
                        Swal.fire(
                            'Success!',
                            'Pertanyaan berhasil diupdate!',
                            'success'
                        )
                    </script>
                @endpush
            @endif
            <a class="btn btn-primary mb-2 float-right" href="{{ route('pertanyaan.create') }}">Create New Pertanyaan</a>
            <table class="table table-bordered text-center">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Judul</th>
                        <th>Isi</th>
                        <th>Tanggal Dibuat</th>
                        <th>Tanggal Diperbaharui</th>
                        <th>ID Jawaban Tepat</th>
                        <th>ID Profil</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse ($pertanyaan as $key => $item)
                        <tr>
                            <td>{{ $key+1 }}</td>
                            <td>{{ $item->judul }}</td>
                            <td>{{ $item->isi }}</td>
                            <td>{{ $item->tanggal_dibuat }}</td>
                            <td>{{ $item->tanggal_diperbaharui }}</td>
                            <td>{{ $item->jawaban_tepat_id }}</td>
                            <td>{{ $item->profil_id }}</td>
                            <td style="display: flex" class="justify-content-around">
                                <a href="{{ route('pertanyaan.show', ['pertanyaan' => $item->id]) }}" class="btn btn-info btn-sm">show</a>
                                <a href="{{ route('pertanyaan.edit', ['pertanyaan' => $item->id], '/edit') }}" class="btn btn-warning btn-sm">edit</a>
                                {{-- <form action="/pertanyaan/{{ $item->id }}" method="post">
                                    @csrf
                                    @method('DELETE')
                                    <input type="submit" value="delete" class="btn btn-danger btn-sm">
                                </form> --}}
                                <button onclick="deleteItem(this)" data-id="{{ $item->id }}" class="btn btn-danger btn-sm">Delete</button>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="8" align="center">No Posts</td>
                        </tr>
                    @endforelse
                </tbody>
            </table>
        </div>
        <!-- /.card-body -->
    </div>
@endsection

<script type="application/javascript">

    function deleteItem(e){

        let id = e.getAttribute('data-id');

        const swalWithBootstrapButtons = Swal.mixin({
            customClass: {
                confirmButton: 'btn btn-success',
                cancelButton: 'btn btn-danger'
            },
            buttonsStyling: false
        });

        swalWithBootstrapButtons.fire({
            title: 'Apakah kamu yakin?',
            text: "Data pertanyaan yang sudah dihapus tidak bisa dikembalikan lagi!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya, hapus!',
            cancelButtonText: 'Batal',
            reverseButtons: true
        }).then((result) => {
            if (result.value) {
                if (result.isConfirmed){

                    $.ajax({
                        type:'DELETE',
                        url:'{{url("/pertanyaan")}}/' +id,
                        data:{
                            "_token": "{{ csrf_token() }}",
                        },
                        success:function(data) {
                            if (data.success){
                                swalWithBootstrapButtons.fire(
                                    'Deleted!',
                                    'Data pertanyaan sudah terhapus.',
                                    "success"
                                ).then(function(){
                                    location.reload();
                                });
                                // $("#"+id+"").remove(); // you can add name div to remove                                
                            }
                        }
                    });
                }
            } else if (
                result.dismiss === Swal.DismissReason.cancel
            ) {
                swalWithBootstrapButtons.fire(
                    'Cancelled',
                    'Data pertanyaan masih aman :)',
                    'error'
                );
            }
        });
    }
</script>