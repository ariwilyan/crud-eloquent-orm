@extends('adminlte.master')

@section('title')
    Form Edit Pertanyaan
@endsection

@section('content')
    <div class="ml-3 mt-3">
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">Edit Pertanyaan ID - {{ $pertanyaan->id }}</h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form role="form" action="{{ route('pertanyaan.update', ['pertanyaan' => $pertanyaan->id]) }}" method="POST">
                @csrf
                @method('PUT')
                <div class="card-body">
                    @error('judul')
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            {{ $message }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    @enderror
                    @error('isi')
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            {{ $message }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    @enderror
                    @error('tanggal_dibuat')
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            {{ $message }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    @enderror
                    @error('tanggal_diperbaharui')
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            {{ $message }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    @enderror
                    <div class="form-group">
                        <label for="judul">Judul Pertanyaan</label>
                        <input type="text" class="form-control" id="judul" name="judul" value="{{ old('judul', $pertanyaan->judul) }}" placeholder="Masukkan Judul">
                        {{-- @error('judul')
                            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                {{ $message }}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        @enderror --}}
                    </div>
                    <div class="form-group">
                        <label for="isi">Isi Pertanyaan</label>
                        <textarea class="form-control" name="isi" id="isi" rows="5" placeholder="Masukkan Isi">{{ old('isi', $pertanyaan->isi) }}</textarea>
                        {{-- @error('isi')
                            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                {{ $message }}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        @enderror --}}
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <!-- textarea -->
                            <div class="form-group">
                                <label for="tanggal_dibuat">Tanggal Dibuat</label>
                                <input type="date" class="form-control" id="tanggal_dibuat" name="tanggal_dibuat"
                                    value="{{ old('tanggal_dibuat', date('Y-m-d', strtotime($pertanyaan->tanggal_dibuat))) }}" placeholder="Masukkan Tanggal Dibuat">
                                {{-- @error('tanggal_dibuat')
                                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                        {{ $message }}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                @enderror --}}
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="tanggal_diperbaharui">Tanggal Diperbaharui</label>
                                <input type="date" class="form-control" id="tanggal_diperbaharui" name="tanggal_diperbaharui" 
                                    value="{{ old('tanggal_diperbaharui', date('Y-m-d', strtotime($pertanyaan->tanggal_diperbaharui))) }}" placeholder="Masukkan Tanggal Diperbaharui">
                                {{-- @error('tanggal_diperbaharui')
                                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                        {{ $message }}
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                @enderror --}}
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="jawaban_tepat_id">ID Jawaban Tepat</label>
                        <input type="number" class="form-control" id="jawaban_tepat_id" name="jawaban_tepat_id"
                            value="{{ old('jawaban_tepat_id', $pertanyaan->jawaban_tepat_id) }}" placeholder="Masukkan ID Jawaban Tepat">
                        {{-- @error('jawaban_tepat_id')
                                <div class="alert alert-danger">{{ $message }}</div>
                    @enderror --}}
                    </div>
                    <div class="form-group">
                        <label for="profil_id">ID Profil</label>
                        <input type="number" class="form-control" id="profil_id" name="profil_id"
                            value="{{ old('profil_id', $pertanyaan->profil_id) }}" placeholder="Masukkan ID Profil">
                        {{-- @error('profil_id')
                                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror --}}
                    </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                    <a class="btn btn-warning mr-1" href="{{ route('pertanyaan.index') }}">Batal</a>
                    <button type="submit" class="btn btn-primary">Update</button>
                </div>
            </form>
        </div>
    </div>
@endsection