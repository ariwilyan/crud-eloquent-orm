<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Pertanyaan;

class PertanyaanController extends Controller
{
    public function index()
    {
        // versi query builder
        // $pertanyaan = DB::table('pertanyaan')->get();

        // versi olequent model orm
        $pertanyaan = Pertanyaan::all();
        return view('pertanyaan.index', compact('pertanyaan'));
    }

    public function create()
    {
        return view('pertanyaan.create');
    }

    public function store(Request $request)
    {
        // dd($request->all());
        $request->validate([
            'judul' => 'required',
            'isi' => 'required',
            'tanggal_dibuat' => 'required',
            'tanggal_diperbaharui' => 'required'
        ]);

        // versi query builder
        // $query = DB::table('pertanyaan')->insert([
        //     'judul' => $request['judul'],
        //     'isi' => $request['isi'],
        //     'tanggal_dibuat' => $request['tanggal_dibuat'],
        //     'tanggal_diperbaharui' => $request['tanggal_diperbaharui'],
        //     'jawaban_tepat_id' => $request['jawaban_tepat_id'],
        //     'profil_id' => $request['profil_id']
        // ]);

        // versi olequent model orm
        $pertanyaan = Pertanyaan::create([
            'judul' => $request['judul'],
            'isi' => $request['isi'],
            'tanggal_dibuat' => $request['tanggal_dibuat'],
            'tanggal_diperbaharui' => $request['tanggal_diperbaharui'],
            'jawaban_tepat_id' => $request['jawaban_tepat_id'],
            'profil_id' => $request['profil_id']
        ]);

        return redirect('/pertanyaan')->with('success', 'Pertanyaan Baru Berhasil Disimpan!');
    }

    public function show($id)
    {
        // versi query builder
        // $pertanyaan = DB::table('pertanyaan')->where('id', $id)->first();

        // versi olequent model orm
        $pertanyaan = Pertanyaan::find($id);

        return view('pertanyaan.show', compact('pertanyaan'));
    }

    public function edit($id)
    {
        // versi query builder
        // $pertanyaan = DB::table('pertanyaan')->where('id', $id)->first();

        // versi olequent model orm
        $pertanyaan = Pertanyaan::find($id);

        return view('pertanyaan.edit', compact('pertanyaan'));
    }

    public function update($id, Request $request)
    {
        $request->validate([
            'judul' => 'required',
            'isi' => 'required',
            'tanggal_dibuat' => 'required',
            'tanggal_diperbaharui' => 'required'
        ]);

        // versi query builder
        // $query = DB::table('pertanyaan')
        //             ->where('id', $id)
        //             ->update([
        //                 'judul' => $request['judul'],
        //                 'isi' => $request['isi'],
        //                 'tanggal_dibuat' => $request['tanggal_dibuat'],
        //                 'tanggal_diperbaharui' => $request['tanggal_diperbaharui'],
        //                 'jawaban_tepat_id' => $request['jawaban_tepat_id'],
        //                 'profil_id' => $request['profil_id']
        //             ]);

        // versi olequent model orm
        $pertanyaan = Pertanyaan::find($id)->update([
            "judul" => $request['judul'],
            'isi' => $request['isi'],
            'tanggal_dibuat' => $request['tanggal_dibuat'],
            'tanggal_diperbaharui' => $request['tanggal_diperbaharui'],
            'jawaban_tepat_id' => $request['jawaban_tepat_id'],
            'profil_id' => $request['profil_id']
        ]);

        return redirect('/pertanyaan')->with('update', 'Berhasil Update Pertanyaan!');
    }

    // public function destroy($id)
    // {
    //     // versi query builder
    //     // $query = DB::table('pertanyaan')->where('id', $id)->delete();

    //     // versi olequent model orm
    //     Pertanyaan::destroy($id);

    //     return redirect('/pertanyaan')->with('delete', 'Pertanyaan Berhasil Dihapus!');
    // }

    public function destroy(Request $request, $id)
    {
        if ($request->ajax()){
            $user = Pertanyaan::findOrFail($id);

            if ($user){
                $user->delete();
                return response()->json(array('success' => true));
            }
        }
    }
}
